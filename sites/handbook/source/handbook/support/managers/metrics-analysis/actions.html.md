---
layout: handbook-page-toc
title: Metrics Analysis - Hypothesis and Actions
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

This page documents hypothesis for metrics dips and techniques for evaluating them.
Additionally it includes specific actions that might be used to shore lagging metrics up.

## FRT is below target

### Hypothesis 1 for FRT

#### Past Analysis
{:.no_toc}

#### Shaping Actions
{:.no_toc}

## SSAT is below target

### Hypothesis 1 for SSAT

#### Past Analysis
{:.no_toc}

#### Shaping Actions
{:.no_toc}

## NRT is below target
### Hypothesis 1 for NRT

#### Past Analysis
{:.no_toc}

#### Shaping Actions
{:.no_toc}


