---
layout: handbook-page-toc
title: "Create:Source Code FE Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Source Code FE team is responsible for all frontend aspects of the product categories that fall under the [Source Code group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/product-categories/#source-code-group
[stage]: /handbook/product/product-categories/#create-stage
[lifecycle]: /handbook/product/product-categories/#devops-stages

## Team Members

The following people are permanent members of the Create:Source Code FE Team:

<%= direct_team(manager_role: 'Frontend Engineering Manager, Create:Source Code, Delivery & Scalability') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Source Code)/, direct_manager_role: 'Frontend Engineering Manager, Create:Source Code', other_manager_roles: ['Frontend Engineering Manager, Create:Source Code, Delivery & Scalability']) %>

## KPIs

<%= partial("handbook/engineering/development/dev/create-source-code-fe/metrics.erb") %>

## Iteration

We held an Iteration Retrospective in April 2020 in order to review past work and discuss how we could improve iteration for upcoming efforts.

* [Frontend: Iteration Retrospective (Source Code)](https://gitlab.com/gl-retrospectives/create-stage/source-code/-/issues/22)

Some overal conclusions/improvements
* Despite having improved the splitting of Merge Requests, we still tend to keep one issue spawning multiple Merge Requests.
* We'll be more strict about splitting the issues in foreseeable iteration steps upon scheduling/assignment
* We must keep in mind the overhead in review times when splitting up related backstage work. ([more info](https://gitlab.com/gl-retrospectives/create-stage/source-code/-/issues/22#note_342547093))

## Work 

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create:Source Code FE team, it's best to create an issue in the relevant project
(typically [GitLab]) and add the `~"group::source code"` and `~frontend` labels, along with any other
appropriate labels (`~devops::create`, `~section::dev`). Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_source_code] or [[#g_create_source_code_fe] on Slack.

[Take a look at the features we support per category here.](/handbook/product/product-categories/features/#createsource-code-group)

[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#g_create_source_code]: https://gitlab.slack.com/archives/g_create_source-code
[#g_create_source_code_fe]: https://gitlab.slack.com/archives/g_create_source-code-fe

## Working with product

Weekly calls between the product manager and engineering managers (frontend and backend) are listed in the "Source Code Group" calendar. Everyone is welcome to join and these calls are used to discuss any roadblocks, concerns, status updates, deliverables, or other thoughts that impact the group. Every 2 weeks (in the middle of a release), a mid-milestone check-in occurs, to report on the current status of ~"Deliverable"s. Monthly calls occurs under the same calendar where the entire group is encouraged to join, in order to highlight accomplishments/improvements, discuss future iterations, review retrospective concerns and action items, and any other general items that impact the group.

### Middle of milestone check-in

Expanding on the concept of Middle of milestone check-in:

The way we try to grasp how well we are doing according to the scheduled and commited set of Deliverables is simply trying to calculate the level of completeness of all of them.

We do this by tallying up:

<dl class="dl-horizontal">
  <dt>Closed/Verification</dt>
  <dd>100% done</dd>

  <dt>In review</dt>
  <dd>80% done</dd>

  <dt>In dev</dt>
  <dd>40% done</dd>

  <dt>Unstarted</dt>
  <dd>0% done</dd>
</dl>

We then compile a small report like this:


<pre>
Done + Verification: 		1 	w1 	2.27%
In review: 		        6 	w15 	34.09%
In dev: 		        6 	w20 	45.45%
Unstarted: 		        3 	w8 	18.18%
Progress: 				        47.73%
Conclusion: ...
</pre>

Progress is calculated with: 

<pre>100% * 2.27 + 80% * 34.09 + 40% * 45.45 + 0% * 18.18</pre>

In the conclusion we write an interpretation of what this means and what we'll be doing to correct course, if needed.

## Capacity planning

We use weights to forecast the complexity of each given issue aimed at being scheduled into a given milestone. These weights help us ensure that the amount of scheduled work in a cycle is reasonable, both for the team as a whole and for each individual. The "weight budget" for a given cycle is determined based on the team's recent output, as well as the upcoming availability of each engineer.

Before each milestone, the Engineering Manager takes a pass and sets weights on all issues currently aimed at the next milestone by Product and triaging processes. On occasion, specific ICs can be brought to review the assigned weight. This is aimed at helping the ICs stay focused on Deliverables while working in the last week of the activ milestone.

We understand weights are mere forecasts and we accept the uncertainty that comes with this.

### Weights

These are the broad definition for each of the weights values. We don't use a Fibonnacci-based progression but we do try to split up large issues as we understand they are by definition less accurate predictions.

When issues are Frontend only, we use the Weight feature of Issues.

When issues are both Frontend and Backend we use specific labels to support both weights in the same issue: `~frontend-weight::1` through `~frontend-weight::8`.

| Weight | Description  |
| --- | --- | --- |
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. |
| 3: Medium | Features that are well understood and relatively straightforward. Bugs that are relatively poorly understood and may not yet have a suggested solution. |
| 4: Medium | Features that are well understood and relatively straightforward but still need a good deal of work. Bugs that are relatively poorly understood and may not yet have a suggested solution. Includes some tasks that might be hard to test. |
| 5: Large | Features that are well understood, but known to be hard. Bugs that are very poorly understood, and will not have a suggested solution. |
| 8: Very large | Very large features that usually require the dedication of a FE for the full length of the milestone. — Very rare, usually split into smaller issues |

## Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::source code' }) %>

## Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

## Kickoff

On the first week of every milestone, we have a sync-call with every IC in which they take turns at presenting their plan for their Deliverables for the new milestone.

This usually happens at the first Thursday of the milestone (at least 5 days into it) at 3PM UTC.

## Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Feature" retrospectives more focused at analyzing a specific case, usually looking into the Iteration approach.

#### Per Milestone

The Create:Source Code group conducts [monthly retrospectives in GitLab issues][retros]. These include
the frontend team, the backend team, UX, and PM who have worked with
that team during the release being retrospected.

These are confidential during the initial discussion, then made public in time
for each month's [GitLab retrospective]. For more information, see [team
retrospectives].

[retros]: https://gitlab.com/gl-retrospectives/create-stage/source-code/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[GitLab retrospective]: /handbook/engineering/workflow/#retrospective
[team retrospectives]: /handbook/engineering/management/team-retrospectives/
